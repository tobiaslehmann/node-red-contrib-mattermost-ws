module.exports = function (RED) {
    const fs = require('fs')

    var handle_error = function (err, node) {
        node.status({
            fill: "red",
            shape: "dot",
            text: err.message
        });
        node.error(err.message);
    };

    function MMUploadNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        node.host = RED.nodes.getNode(config.host);
        node.files = config.files;

        node.uploadFiles = function (msg, files, channel_id) {
            if (files.length > 0) {
              var files_data = [];
              for(let i = 0; i < files.length; i++){
                let file = files[i].trim();
                if (fs.existsSync(file)) {
                  files_data.push(fs.createReadStream(file,{flags:'r'}));
                } else {
                  return handle_error(new Error(`Cannot access file ${file}`), node);
                }
              }

              const form_data = {
                channel_id: channel_id,
                files: files_data
              };

              node.status({
                  fill: "green",
                  shape: "dot",
                  text: `Uploading...`
              });
              setTimeout(function () {
                  node.status({
                      fill: "blue",
                      shape: "dot",
                      text: `Ready`
                  });
              }, 500);


              return node.host.client.uploadFile(channel_id, files_data, (data) => {
                  node.host.client.logger.debug('Posted file');
                  msg.payload = data || false;
                  msg.file_ids = [];
                  for (let i = 0; i < msg.payload.file_infos.length; i++){
                      msg.file_ids[i]=msg.payload.file_infos[i].id;
                  }
                  node.send(msg);
                  node.status({
                      fill: "green",
                      shape: "dot",
                      text: `Sent!`
                  });
              });

            } else {
              return handle_error(new Error(`No files specified to upload`), node);
            }
        };
        
        //handle message in
        node.on('input', function (msg) {

            if (!node.host.client) {
                node.host.createClient();
            }
            if (!node.host.client.isConnected()) {
                node.status({
                    fill: "red",
                    shape: "dot",
                    text: `Error logging in!`
                });
                return handle_error(new Error("Error logging in!"), node);
           }

            node.status({
                fill: "orange",
                shape: "dot",
                text: `Trying to upload files`
            });

            var channelName = msg.channel ? msg.channel : node.host.defaultChannel;

            var files = [];
            if (msg.files) {
              files = msg.files;
            } else if (node.files) {
              files = node.files.split(",");
            }

            if (channelName.startsWith("@")) {
              node.log("sending file(s) to "+channelName);
              channelName = channelName.substr(1);
              var user = node.host.client.getUserByUsername(channelName);
              if (!user)
                  user = node.host.client.getUserByEmail(channelName);

              if (user)
                  return node.host.client.getUserDirectMessageChannel(user.id, function (privateChannel) {
                      node.uploadFiles(msg, files, privateChannel.id);
                  });

              return handle_error(new Error(`User ${channelName} cannot be found`), node);
            } else {
              // try by id
              if (node.host.client.channels[channelName])
                  return node.uploadFiles(msg, files, node.host.client.channels[channelName].id);

              // by channel name
              var channel = node.host.client.findChannelByName(channelName);
              if (channel)
                  return node.uploadFiles(msg, files, channel.id);
            }

            return handle_error(new Error(`Channel ${channelName} cannot be found`), node);
        });
    }
    RED.nodes.registerType("mm-upload", MMUploadNode);
};

